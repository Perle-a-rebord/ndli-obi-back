const config = require("../config/jwt");
const jwt = require("jsonwebtoken");

module.exports = async (req, res, next) => {
  if (req.path === "/users/login") {
    return next();
  }
  const authHeader = req.headers.authorization;

  if (authHeader) {
    jwt.verify(authHeader, config.jwt.secret, (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }
      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};

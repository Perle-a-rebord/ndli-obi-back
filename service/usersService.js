const { query, getByLogin, createUser } = require("../dao/usersRepository");
const config = require("../config/jwt");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const saltRounds = 10;

async function getAll() {
  return await query();
}

async function create(user) {
  user.password = await bcrypt.hash(user.password, saltRounds);
  return await createUser(user);
}

async function login(login, password) {
  const user = await getByLogin(login);
  const matchPwd = await bcrypt.compare(password, user.password);
  // Generate an access token
  return matchPwd
    ? jwt.sign({ login: user.login, role: user.role }, config.jwt.secret)
    : {};
}

module.exports = {
  getAll,
  login,
  create,
};

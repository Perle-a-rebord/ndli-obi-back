const mysql = require("mysql2/promise");
const config = require("../config/mysql");
const users = require("../assets/users.json");

async function query() {
  const connection = await mysql.createConnection(config.db);
  const [results] = await connection.query("SELECT * FROM t_user_use");
  connection.end();
  return results;
}

async function createUser(user) {
  const connection = await mysql.createConnection(config.db);
  const [results] = await connection.query(
    `INSERT INTO t_user_use (use_prenom, use_login, use_password)
  VALUES(?, ?, ?)`,
    [user.prenom, user.login, user.password]
  );
  connection.end();
  return results;
}

async function getByLogin(login) {
  const connection = await mysql.createConnection(config.db);
  const [results] = await connection.query(
    `SELECT * FROM t_user_use WHERE login = ?`,
    [login]
  );
  connection.end();
  return results[0];
}

module.exports = {
  query,
  getByLogin,
  createUser,
};


const express = require('express');
const usersRouter = require('./routes/users');
const interventionsRouter = require('./routes/interventions');
const personnesInterRouter = require('./routes/personnesInter');
const bateauNomRouter= require('./routes/bateauNom');
const personnesRouter= require('./routes/personnes');
const listeDateRouter= require('./routes/listeDate');
const interBateauRouter= require('./routes/interBateau');
const bodyParser = require('body-parser');
const authenticate = require('./middlewares/authenticate')
const app = express();
//Handles post requests
app.use(bodyParser.json());

// app.use('/', authenticate);
app.use('/users', usersRouter);
app.use('/interventions', interventionsRouter);
app.use('/personnesInter', personnesInterRouter);
app.use('/bateauNom', bateauNomRouter);
app.use('/personnes', personnesRouter);
app.use('/listeDate', listeDateRouter);
app.use('/interBateau', interBateauRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(3000)

module.exports = app;
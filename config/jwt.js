const env = process.env;

const config = {
    jwt: {
      secret: env.JWT_SECRET || 'secret_token',
    },
  };
  
  module.exports = config;
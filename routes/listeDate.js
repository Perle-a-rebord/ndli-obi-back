const express = require("express");
const router = express.Router();
const mysql = require("mysql2/promise");
const config = require("../config/mysql");

router.get("/:lieu:date", async function (req, res, next) {
    const connection = await mysql.createConnection(config.db);
    const [results] = await connection.query("SELECT int_date,int_lieu FROM `t_intervention_int` WHERE int_lieu LIKE ?", ["%" + req.params.lieu + "%"]," AND int_date LIKE ?", ["%" + req.params.date + "%"]);
    connection.end();
    res.send(results);
  });

  module.exports = router;
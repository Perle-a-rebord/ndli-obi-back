const express = require("express");
const router = express.Router();
const mysql = require("mysql2/promise");
const config = require("../config/mysql");

router.get("/:prenom", async function (req, res, next) {
    const connection = await mysql.createConnection(config.db);
    
    const [results] = await connection.query("SELECT per_prenom,per_nom,int_date,int_lieu from t_intervention_int LEFT JOIN tj_sauver_sau using(int_id) LEFT JOIN t_personne_per using(per_id) WHERE per_prenom LIKE ?", ["%" + req.params.prenom + "%"]);
    connection.end();
    res.send(results);
  });

  module.exports = router;
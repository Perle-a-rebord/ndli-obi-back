const express = require("express");
const router = express.Router();
const { login, getAll, create } = require("../service/usersService");
const mysql = require("mysql2/promise");
const config = require("../config/mysql");

router.post("/login", async function (req, res, next) {
  const token = await login(req.body.login, req.body.password);
  if (token) {
    res.json({
      token: token,
    });
  } else {
    res.send("login or password incorrect");
  }
});

router.post("/", async function (req, res, next) {
  res.send(await create(req.body));
});

/* GET users listing. */
router.get("/", async function (req, res, next) {
  const connection = await mysql.createConnection(config.db);
  const [results] = await connection.query("SELECT * FROM t_user_use");
  connection.end();
  res.send(results);
});

module.exports = router;

const express = require("express");
const router = express.Router();
const mysql = require("mysql2/promise");
const config = require("../config/mysql");

router.get("/", async function (req, res, next) {
    const connection = await mysql.createConnection(config.db);
    let query = "SELECT bat_nom,bat_patron from t_bateau_bat";
    let params = [];
    if(req.query.libelle){
        query = "SELECT bat_nom,bat_patron from t_bateau_bat WHERE bat_nom LIKE ?";
        params.push(req.query.libelle);
    }
    const [results] = await connection.query(query, params);
    connection.end();
    res.send(results);
  });

  router.get("/:libelle", async function (req, res, next) {
    const connection = await mysql.createConnection(config.db);
    const [results] = await connection.query("SELECT bat_nom,bat_patron from t_bateau_bat WHERE bat_nom LIKE ?", ["%" + req.params.libelle + "%"]);
    connection.end();
    res.send(results);
  });


  module.exports = router;
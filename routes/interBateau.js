const express = require("express");
const router = express.Router();
const mysql = require("mysql2/promise");
const config = require("../config/mysql");

router.get("/:nomBateau", async function (req, res, next) {
    const connection = await mysql.createConnection(config.db);
    const [results] = await connection.query("SELECT bat_nom, bat_patron, int_date, int_lieu FROM t_bateau_bat JOIN t_intervention_int USING(bat_id) WHERE bat_nom LIKE ?", ["%" + req.params.nomBateau + "%"]);
    connection.end();
    res.send(results);
  });

  module.exports = router;
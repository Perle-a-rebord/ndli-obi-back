const express = require("express");
const router = express.Router();
const mysql = require("mysql2/promise");
const config = require("../config/mysql");

router.get("/", async function (req, res, next) {
    const connection = await mysql.createConnection(config.db);
    const [results] = await connection.query("SELECT per_nom,per_prenom FROM t_personne_per JOIN tj_sauver_sau USING(per_id) ORDER BY int_id");
    connection.end();
    res.send(results);
  });

  module.exports = router;